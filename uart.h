/*
 * uart.h
 *
 * Created: 2-5-2019 14:24:25
 *  Author: renee
 */ 


#ifndef UART_H_
#define UART_H_
#include <avr/io.h>

void USART_Init (unsigned int ubrr);
void USART_Transmit (unsigned char data);


#endif /* UART_H_ */