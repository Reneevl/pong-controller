/*
 * battery.h
 *
 * Created: 2-5-2019 15:52:02
 *  Author: renee
 */ 


#ifndef BATTERY_H_
#define BATTERY_H_
#include <util/delay.h>
#include <avr/io.h>

void init_battery();
void read_battery();
void set_leds(double voltage);
uint16_t ReadADC(uint8_t __channel);
void adc_init();



#endif /* BATTERY_H_ */