/*
 * battery.c
 *
 * Created: 2-5-2019 15:51:45
 *  Author: renee
 */ 
#define MAX 4.2
#define MIN 2.8
#include "battery.h"

void init_battery(){
	//max battery voltage is 4,2 min 2,8-2,5 on PC2
	  DDRB |= (1 << DDB6);	//red led output
	  DDRC |= (1 << DDC0);  //green led output
	  DDRC |= (1 << DDC1);	//yellow led output
	  
	  PORTB &= ~(1 << PINB6); //set red low
	  PORTC &= ~(1 << PINC0); //set green low
	  PORTC &= ~(1 << PINC1); //set yelLOW  
	  
	  DDRB |= (1 << DDB2);	//powerled output
	  PORTB |= (1 << PINB2); //powerled high
	  
	  adc_init();
	 
}

void read_battery(){
	  _delay_ms(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring

  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH  
  uint8_t high = ADCH; // unlocks both

  double result = (high<<8) | low;

  result = (result/1024) * 3.3; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
 
	  set_leds(result);
}

void set_leds(double voltage){
	if(voltage <= 1.6)
	{
	  PORTB |= (1 << PINB6); //set red high
	  PORTC |= (1 << PINC0); //set green low
	  PORTC |= (1 << PINC1); //set yelLOW
	}else if(voltage > 1.6 && voltage < 2)
	{
	  PORTB |= (1 << PINB6); //set red low
	  PORTC |= (1 << PINC0); //set green low
	  PORTC |= (1 << PINC1);  //set yelHIGH		
	}else
	{
	  PORTB |= (1 << PINB6); //set red low
	  PORTC |= (1 << PINC0);  //set green high
	  PORTC |= (1 << PINC1); //set yelLOW
	}
}

uint16_t ReadADC(uint8_t __channel)
{
   ADMUX |= __channel;                // Channel selection
   ADCSRA |= _BV(ADSC);               // Start conversion
   while(!bit_is_set(ADCSRA,ADIF));   // Loop until conversion is complete
   ADCSRA |= _BV(ADIF);               // Clear ADIF by writing a 1 (this sets the value to 0)

   return(ADC);
}

void adc_init()
{
	ADCSRA = (1 << ADEN) | (1 << ADPS2) |(1 << ADPS1) | (1 << ADPS0); //Enable ADC and set 128 prescale
	ADMUX = (1 << REFS0) | (1 << MUX1); //adc2 with reference	
}