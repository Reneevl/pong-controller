/*
 * uart.c
 *
 * Created: 2-5-2019 14:24:08
 *  Author: renee
 */ 
#include "uart.h"


// These are really useful macros that help to get rid of unreadable bit masking code
#define setBit(reg, bit) (reg = reg | (1 << bit))
#define clearBit(reg, bit) (reg = reg & ~(1 << bit))
#define toggleBit(reg, bit) (reg = reg ^ (1 << bit))
#define clearFlag(reg, bit) (reg = reg | (1 << bit))


void USART_Init (unsigned int ubrr)
{
	// reset register
	UCSR0A = 0x0;
	UCSR0B = 0x0;
	UCSR0C = 0x0;
	
	/* Set baud rate */
	UCSR0A = (1<< U2X0);
	UBRR0H = (unsigned char)(ubrr>>8);
	UBRR0L = (unsigned char)ubrr;
	/* Enable transmitter and transmit interrupt*/
	UCSR0B = (1<<TXEN0);
	/* Set frame format: 8data, 1stop bit */
	UCSR0C = (3<<UCSZ00);
	
		// Initialize Registers
	
	// Configure register UCSRA
	setBit(UCSR0A, U2X0);				// Double the BRG speed (since I am using a 8MHz crystal which is divided by 8)
	clearBit(UCSR0A, MPCM0);			// Normal UART communication

	setBit(UCSR0B, TXEN0);				// Enable transmission
	clearBit(UCSR0B, UCSZ02);			// 8 bit character size

	// Configure register UCSRC
	clearBit(UCSR0C, UMSEL00);			// Normal Asynchronous Mode
	clearBit(UCSR0C, UMSEL01);
	clearBit(UCSR0C, UPM00);			// No Parity Bits
	clearBit(UCSR0C, UPM01);			// No Parity Bits
	clearBit(UCSR0C, USBS0);			// Use 1 stop bit
	setBit(UCSR0C, UCSZ01);				// 8 bit character size
	setBit(UCSR0C, UCSZ00);

}

void USART_Transmit (unsigned char data)
{
	/* Wait for empty transmit buffer */
	while (!(UCSR0A & (1<<UDRE0)))
	;
	/* Put data into buffer, sends the data */
	UDR0 = data;
}
