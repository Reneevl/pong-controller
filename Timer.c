/*
 * Timer.c
 *
 * Created: 04/06/2019 09:32:36
 *  Author: Remco
 */ 
#include "timer.h"

void timer_init()
{
	//zeg even tcnp0 count register
	//tifr0 timer flag register
	//timska0 timer mask register.
	TCNT1 = 0;
	TCCR1A = (1 << COM1A1) | (1 << COM1A0) | (1 << WGM12);
	TCCR1B = (1 << CS10) | (1 << CS12);
	OCR1A = 1562;
	TIMSK1 = (1 << OCIE1A) | (1 << TOIE1);	
}

