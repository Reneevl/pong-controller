#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>    // Needed to use interrupts
#include <math.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#include "buttons.h"
#include "uart.h"
#include "battery.h"

#define MPU6050_GETATTITUDE 1

#include "mpu6050_old.h"
#include "timer.h"
//#include "mpu6050.h"

#define FOSC 20000000// Clock Speed

#define BAUD 9600
#define MYUBRR F_CPU/16/BAUD-1

volatile int portbhistory = 0x00;

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>


const float gyro_coef = 0.90;
const float acc_coef = 0.1;
float accel_x;
float accel_y;
float accel_z;
	
float gyro_x;
float gyro_y;
float gyro_z;

float cali_gyro_x;
float cali_gyro_y;
float cali_gyro_z;
	
float angle_x;
float angle_y;
float angle_z;

float interval = 0.001;


double angle_accel_x;
double angle_accel_y;


void transmitter_init()
{
	USART_Transmit(0xA);
	USART_Transmit(0xB);
	USART_Transmit(0xC);
	USART_Transmit(0xD);
}

long *ptr = 0;
double qw = 1.0f;
double qx = 0.0f;
double qy = 0.0f;
double qz = 0.0f;
double roll = 0.0f;
double pitch = 0.0f;
double yaw = 0.0f;

double test = 0;

//#define MPU6050_GETATTITUDE 1

uint8_t cmd = 0;

int main(void)
{
	init_buttons();
	USART_Init (12);
	init_battery();
	TWI_init();
	timer_init();
	
	sei();
	USART_Transmit(0xA);
	USART_Transmit(0xB);
	USART_Transmit(0xC);
	USART_Transmit(0xD);
	
	read_battery();
	MPU6050_init();
	//mpu6050_init();
	_delay_us(50);
	
//	mpu6050_dmpInitialize();
//	mpu6050_dmpEnable();
//_delay_ms(10);
		cali_gyro_x = ((float)MPU6050_get_gyroX()) / GYRO_DIVIDER_FS_SEL_0;
		cali_gyro_y = ((float)MPU6050_get_gyroY()) / GYRO_DIVIDER_FS_SEL_0;
		//cali_gyro_z = ((float)MPU6050_get_gyroZ()) / GYRO_DIVIDER_FS_SEL_0;
	while(1)
	{	
	/*	
		int16_t x = MPU6050_get_accelerationX();
		accel_x = ((float)x) / 16384;
		accel_y = ((float)MPU6050_get_accelerationY()) / 16384;
		accel_z = ((float)MPU6050_get_accelerationZ()) / 16384;
		
		gyro_x = ((float)MPU6050_get_gyroX()) / GYRO_DIVIDER_FS_SEL_0 - cali_gyro_x;
		gyro_y = ((float)MPU6050_get_gyroY()) / GYRO_DIVIDER_FS_SEL_0;// - cali_gyro_y;
		gyro_z = ((float)MPU6050_get_gyroZ()) / GYRO_DIVIDER_FS_SEL_0;
		
		test =atan2(accel_y, accel_z + abs(accel_x));
		angle_accel_x = test * 360 / (2 * M_PI);
		_delay_ms(1);
		
		test = atan2(accel_x, accel_z + abs(accel_y));
		angle_accel_y =test* 360 / (-2*M_PI);
		
		angle_x = 0.9 * ( angle_x + gyro_x * interval) + 0.1 * angle_accel_x;
		angle_y = 0.95 * ( angle_y + gyro_y * interval) + 0.05 * angle_accel_y;
		angle_z += accel_z;
		*/
		//USART_Transmit(0x80);
	
		if(angle_x > 30)
			cmd |= 0b00000010;
		else if(angle_x < -30)
			cmd |= 0b00000001;
				
				
		if(angle_y > 70)
			cmd |= 0b00000100;
		else if(angle_y < -70)
			cmd |= 0b00001000;
			
		//if(cmd > 0)
		USART_Transmit(cmd);
		
		cmd = 0;
		_delay_ms(1);		
	}
}


ISR(USART_TX_vect)
{
	UCSR0A = (1 << TXC0);
}

ISR (PCINT2_vect)
{
	int i = 0;
	uint8_t changedbits;
	
	changedbits = PIND ^ portbhistory;
	portbhistory = PIND;

	
	if(changedbits & (1 << PIND0) && PIND & (1 << PIND0))
	{
		USART_Transmit(0x65);
	}
	
	if(changedbits & (1 << PIND2) && PIND & (1 << PIND2))
	{
		USART_Transmit(0x66);
	}

	if(changedbits & (1 << PIND3) && PIND & (1 << PIND3))
	{
		USART_Transmit(0x67);
	}
	if(changedbits & (1 << PIND4) && PIND & (1 << PIND4))
	{
		USART_Transmit(0x68);
	}
	if(changedbits & (1 << PIND5) && PIND & (1 << PIND5))
	{
		USART_Transmit(0x69);
	}
	if(changedbits & (1 << PIND6) && PIND & (1 << PIND6))
	{
		USART_Transmit(0x70);
	}
	
	PCIFR |= (1 << PCIF2);
}

ISR(TIMER1_OVF_vect)
{
	read_battery();
	TCNT1 = 100;	
}