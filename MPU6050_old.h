/*
 * MPU6050.h
 *
 * Created: 20/05/2019 15:23:26
 *  Author: Tom
 */ 


#ifndef MPU6050_H_
#define MPU6050_H_

#include <util/delay.h>
#include <avr/io.h>

#define MPU6050_REG_XG_OFFS_TC                0x00 //[7] PWR_MODE, [6:1] XG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_REG_YG_OFFS_TC                0x01 //[7] PWR_MODE, [6:1] YG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_REG_ZG_OFFS_TC                0x02 //[7] PWR_MODE, [6:1] ZG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_REG_XGYRO_OFFS_USRH           0x13 //[15:0] XG_OFFS_USR
#define MPU6050_REG_XGYRO_OFFS_USRL           0x14
#define MPU6050_REG_YGYRO_OFFS_USRH           0x15 //[15:0] YG_OFFS_USR
#define MPU6050_REG_YGYRO_OFFS_USRL           0x16
#define MPU6050_REG_ZGYRO_OFFS_USRH           0x17 //[15:0] ZG_OFFS_USR
#define MPU6050_REG_SAMPLE_DIVIDER            0x19
#define MPU6050_REG_CONFIGURATION_CONFIG      0x1A
#define MPU6050_REG_GYRO_CONIFG               0x1B
#define MPU6050_REG_ACC_CONFIG                0x1C
#define MPU6050_REG_FF_THR                    0x1D
#define MPU6050_REG_FF_DUR                    0x1E
#define MPU6050_REG_MOT_THR                   0x1F
#define MPU6050_REG_MOT_DUR                   0x20
#define MPU6050_REG_ZRMOT_THR                 0x21
#define MPU6050_REG_ZRMOT_DUR                 0x22
#define MPU6050_REG_FIFO_ENABLE               0x23
#define MPU6050_REG_I2C_MST_CTRL              0x24
#define MPU6050_REG_I2C_SLV0_ADDR             0x25
#define MPU6050_REG_I2C_SLV1_ADDR             0x26
#define MPU6050_REG_I2C_SLV0_CTRL             0x27
#define MPU6050_REG_I2C_SLV1_ADDR             0x28
#define MPU6050_REG_I2C_SLV1_REG              0x29
#define MPU6050_REG_I2C_SLV1_CTRL             0x2A
#define MPU6050_REG_I2C_SLV2_ADDR             0x2B
#define MPU6050_REG_I2C_SLV2_REG              0x2C
#define MPU6050_REG_I2C_SLV2_CTRL             0x2D
#define MPU6050_REG_I2C_SLV3_ADDR             0x2E
#define MPU6050_REG_I2C_SLV3_REG              0x2F
#define MPU6050_REG_I2C_SLV3_CTRL             0x30
#define MPU6050_REG_I2C_SLV4_ADDR             0x31
#define MPU6050_REG_I2C_SLV4_REG              0x32
#define MPU6050_REG_I2C_SLV4_DO               0x33
#define MPU6050_REG_I2C_SLV4_CTRL             0x34
#define MPU6050_REG_I2C_SLV4_DI               0x35
#define MPU6050_REG_I2C_MST_STATUS            0x36

#define MPU6050_REG_ACCEL_XOUT_H			  0x3B
#define MPU6050_REG_ACCEL_XOUT_L			  0x3C

#define MPU6050_REG_ACCEL_YOUT_H			  0x3D
#define MPU6050_REG_ACCEL_YOUT_L			  0x3E

#define MPU6050_REG_ACCEL_ZOUT_H			  0x3F
#define MPU6050_REG_ACCEL_ZOUT_L			  0x40

#define MPU6050_REG_GYRO_XOUT_H				  0x43
#define MPU6050_REG_GYRO_XOUT_L				  0x44

#define MPU6050_REG_GYRO_YOUT_H				  0x45
#define MPU6050_REG_GYRO_YOUT_L				  0x46

#define MPU6050_REG_GYRO_ZOUT_H				  0x47
#define MPU6050_REG_GYRO_ZOUT_L				  0x48

#define MPU6050_REG_I2C_MST_DELAY_CTRL        0x57
#define MPU6050_REG_SIGNAL_PATH_RESET         0x68
#define MPU6050_REG_USER_CTRL                 0x6A
#define MPU6050_REG_PWR_MGMT_1                0x6B
#define MPU6050_REG_PWR_MGMT_2                0x6C

/*
 there is a gap in the listed registers from 109 to 113 and these concern the DMP
 http://www.robotrebels.org/index.php?topic=318.0
*/
// DMP registers
#define MPU6050_REG_BANK_SEL_ADDR             0x6D
#define MPU6050_REG_BANK_MEM_START_ADDR       0x6E
#define MPU6050_RA_MEM_R_W					  0x6F
#define MPU6050_REG_PROG_START_ADDR           0x70

#define MPU6050_REG_FIFO_COUNT_H              0x72
#define MPU6050_REG_FIFO_COUNT_L              0x73
#define MPU6050_REG_FIFO_R_W                  0x74
#define MPU6050_REG_WHO_AM_I                  0x75

#define MPU6050_DMP_CODE_SIZE                 1929    // dmpMemory[]
#define MPU6050_DMP_CONFIG_SIZE               192     // dmpConfig[]
#define MPU6050_DMP_UPDATES_SIZE              47      // dmpUpdates[]


typedef struct
{
	uint8_t clk_source;
	uint8_t sample_divider;
} MPU6050_settings;


void MPU6050_reset();
void MPU6050_sleep_mode();
int8_t MPU6050_DMP_init();
void MPU6050_set_memory_bank(uint8_t bank, uint8_t prefetchEnable, uint8_t userbank);
int8_t MPU6050_load_dmp_into_memory();
uint8_t MPU6050_reset_memory_bank();
uint8_t MPU6050_getHardwareRevision();
void MPU6050_set_memory_start_addr(uint8_t addr);
uint8_t MPU6050_read_memory_byte();
int8_t MPU6050_init();
void MPU6050_set_clock_source(uint8_t);
void MPU6050_set_gyro_full_scale_range(uint8_t fs_sel);
void MPU6050_set_accel_full_scale_range(uint8_t afs_sel);
void MPU6050_set_sleep_mode(uint8_t sleep);
uint8_t MPU6050_get_device_id();

int16_t MPU6050_get_accelerationX();
int16_t MPU6050_get_accelerationY();
int16_t MPU6050_get_accelerationZ();

int16_t MPU6050_get_gyroX();
int16_t MPU6050_get_gyroY();
int16_t MPU6050_get_gyroZ();

int16_t MPU6050_get_raw_accel_x();
int16_t MPU6050_get_raw_accel_y();
int16_t MPU6050_get_raw_accel_z();

int16_t MPU6050_get_raw_gyro_x();
int16_t MPU6050_get_raw_gyro_y();
int16_t MPU6050_get_raw_gyro_z();

#define GYRO_DIVIDER_FS_SEL_0 131;

#endif /* MPU6050_H_ */