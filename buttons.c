/*
 * buttons.c
 *
 * Created: 2-5-2019 14:17:23
 *  Author: renee
 */ 
#include "buttons.h"


void init_buttons()
{
		DDRD &= ~(1 << DDD0);	//pin PD0 (Button_links) is input
		DDRD &= ~(1 << DDD2);	//pin PD2 (Button_onder) is input
		DDRD &= ~(1 << DDD3);	//pin PD3 (Button_boven) is input
		DDRD &= ~(1 << DDD4);	//pin PD4 (Button_OK/pauze) is input
		DDRD &= ~(1 << DDD5);	//pin PD5 (Button_cancel/home) is input
		DDRD &= ~(1 << DDD6);	//pin PD6 (Button_rechts) is input
		
		PCICR |= (1 << PCIE2);	//enable PC16-23
		
		
		PCMSK2 |= (1 << PCINT16);	// set PCINT16 to trigger an interrupt on state change
		PCMSK2 |= (1 << PCINT18);	// set PCINT18 to trigger an interrupt on state change
		PCMSK2 |= (1 << PCINT19);	// set PCINT19 to trigger an interrupt on state change
		PCMSK2 |= (1 << PCINT20);	// set PCINT20 to trigger an interrupt on state change
		PCMSK2 |= (1 << PCINT21);	// set PCINT21 to trigger an interrupt on state change
		PCMSK2 |= (1 << PCINT22);	// set PCINT22 to trigger an interrupt on state change
	
}