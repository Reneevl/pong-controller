/*
 * MPU6050.c
 *
 * Created: 20/05/2019 15:23:11
 *  Author: Tom
 */ 

#include "twi.h"
#include "MPU6050_old.h"
#include <time.h> 

static uint8_t devAddr = 0x68;

static int16_t raw_gyro_x;
static int16_t raw_gyro_y;
static int16_t raw_gyro_z;

static int16_t raw_accel_x;
static int16_t raw_accel_y;
static int16_t raw_accel_z;

void MPU6050_reset()
{
	// MPU6050_REG_USER_CTRL -> reset
	TWI_start();
	TWI_write(devAddr << 1);
	TWI_write(MPU6050_REG_PWR_MGMT_1);
	TWI_write(0x80);
	TWI_stop();
}

void MPU6050_update()
{
	uint8_t buffer[14];
	TWI_read_register(devAddr, MPU6050_REG_ACCEL_XOUT_H, buffer, 14);
	
	raw_accel_x = buffer[0] << 8 | buffer[1];
	raw_accel_y = buffer[2] << 8 | buffer[3];
	raw_accel_z = buffer[4] << 8 | buffer[5];

	raw_gyro_x = buffer[8] << 8 | buffer[9];
	raw_gyro_y = buffer[10] << 8 | buffer[11];
	raw_gyro_z = buffer[12] << 8 | buffer[13];
}

int16_t MPU6050_get_raw_gyro_x()
{
	return (int16_t)raw_gyro_x;
}

int16_t MPU6050_get_raw_gyro_y()
{
	return (int16_t)raw_gyro_y;
}

int16_t MPU6050_get_raw_gyro_z()
{
	return (int16_t)raw_gyro_z;
}

int16_t MPU6050_get_raw_accel_x()
{
	return (int16_t)raw_accel_x;
}

int16_t MPU6050_get_raw_accel_y()
{
	return (int16_t)raw_accel_y;
}

int16_t MPU6050_get_raw_accel_z()
{
	return (int16_t)raw_accel_z;
}

uint8_t MPU6050_read_memory_byte()
{
	uint8_t buff[2];
	//twi_read(devAddr, buff, 1);
	return buff[0];
}

void MPU6050_set_memory_bank(uint8_t bank, uint8_t prefetchEnable, uint8_t userbank)
{
	bank &= 0x1F;
	
	if (userbank == 1) 
		bank |= 0x20;
	
	if (prefetchEnable == 1)
		bank |= 0x40;
	
	TWI_start();
	TWI_write(0x68 << 1);
	TWI_write(MPU6050_REG_BANK_SEL_ADDR);
	TWI_write(bank);
	TWI_stop();
}

uint8_t MPU6050_reset_memory_bank()
{
	uint8_t buffer[2];
	TWI_read_register(devAddr, MPU6050_RA_MEM_R_W, buffer, 2);
	return buffer[0];
}

void MPU6050_set_memory_start_addr(uint8_t addr)
{	
	TWI_start();
	TWI_write(0x68 << 1);
	TWI_write(MPU6050_REG_BANK_MEM_START_ADDR);
	TWI_write(addr);
	TWI_stop();
}

uint8_t MPU6050_getHardwareRevision()
{
	MPU6050_set_memory_bank(0x10,1,1);
	MPU6050_set_memory_start_addr(0x06);
	uint8_t hwRevision = MPU6050_read_memory_byte();
	
	MPU6050_reset_memory_bank();
	
	return hwRevision;
}



int8_t MPU6050_DMP_init()
{
	MPU6050_reset();
	MPU6050_sleep_mode(0);
	uint8_t hw_revision = MPU6050_getHardwareRevision();
	
	// check OTP bank valid
	
	// get x / y / z
}

int8_t MPU6050_init()
{
	MPU6050_reset();
	
	_delay_us(100);

	MPU6050_set_clock_source(0x01);
	MPU6050_set_config(0x00);
	MPU6050_set_gyro_full_scale_range(0x03);
	MPU6050_set_accel_full_scale_range(0x00);	
	MPU6050_set_sample_divider(0x00);
	MPU6050_set_sleep_mode(0);
	MPU6050_get_device_id();
}

void MPU6050_set_config(uint8_t config)
{
	TWI_start();
	TWI_write(devAddr << 1);
	TWI_write(MPU6050_REG_CONFIGURATION_CONFIG);
	TWI_write(config);
	TWI_stop();
}

void MPU6050_set_sample_divider(uint8_t SMPLR_DIV)
{
	TWI_start();
	TWI_write(devAddr  << 1);
	TWI_write(MPU6050_REG_SAMPLE_DIVIDER);
	TWI_write(SMPLR_DIV);
	TWI_stop();
}

void MPU6050_set_clock_source(uint8_t clksel)
{
	clksel &= 0x3; // CLKSEL [2:0]
	
	TWI_start();
	TWI_write(devAddr << 1);
	TWI_write(MPU6050_REG_PWR_MGMT_1);
	TWI_write(clksel);
	TWI_stop();
}

void MPU6050_set_gyro_full_scale_range(uint8_t fs_sel)
{
	fs_sel &= 0x3;
	
	TWI_start();
	TWI_write(devAddr << 1);
	TWI_write(MPU6050_REG_GYRO_CONIFG);
	TWI_write(fs_sel);
	TWI_stop();
}

void MPU6050_set_accel_full_scale_range(uint8_t afs_sel)
{
	afs_sel &= 0x3;
	
	TWI_start();
	TWI_write(devAddr << 1);
	TWI_write(MPU6050_REG_GYRO_CONIFG);
	TWI_write(afs_sel);
	TWI_stop();
}

void MPU6050_set_sleep_mode(uint8_t sleep)
{
	TWI_start();
	TWI_write(devAddr << 1);
	TWI_write(MPU6050_REG_PWR_MGMT_1);
	TWI_write(sleep << 6);
	TWI_stop();	
}

uint8_t MPU6050_get_device_id()
{
	uint8_t data[1];
	TWI_read_register(devAddr, MPU6050_REG_WHO_AM_I,data, 2);
	return data[0];
}

int16_t MPU6050_get_accelerationX()
{
	uint8_t buffer[2];
	uint8_t* buffer_ptr = buffer;
	TWI_read_register(devAddr, MPU6050_REG_ACCEL_XOUT_H, buffer_ptr, 1);
	buffer_ptr++;
	TWI_read_register(devAddr, MPU6050_REG_ACCEL_XOUT_L, buffer_ptr, 1);
	
	return (buffer[0] << 8 | buffer[1]);
}

int16_t MPU6050_get_accelerationY()
{
	uint8_t buffer[2];
	uint8_t* buffer_ptr = buffer;
	TWI_read_register(devAddr, MPU6050_REG_ACCEL_YOUT_H, buffer_ptr, 1);
	buffer_ptr++;
	TWI_read_register(devAddr, MPU6050_REG_ACCEL_YOUT_L, buffer_ptr, 1);
	
	return (buffer[0] << 8 | buffer[1]);
}

int16_t MPU6050_get_accelerationZ()
{
	uint8_t buffer[2];
	uint8_t* buffer_ptr = buffer;
	TWI_read_register(devAddr, MPU6050_REG_ACCEL_ZOUT_H, buffer_ptr, 1);
	buffer_ptr++;
	TWI_read_register(devAddr, MPU6050_REG_ACCEL_ZOUT_L, buffer_ptr, 1);
	
	return (buffer[0] << 8 | buffer[1]);
}


int16_t MPU6050_get_gyroX()
{
	uint8_t buffer[2];
	uint8_t* buffer_ptr = buffer;
	TWI_read_register(devAddr, MPU6050_REG_GYRO_XOUT_H, buffer_ptr, 1);
	buffer_ptr++;
	TWI_read_register(devAddr, MPU6050_REG_GYRO_XOUT_L, buffer_ptr, 1);
	
	return (buffer[0] << 8 | buffer[1]);
}

int16_t MPU6050_get_gyroY()
{
	uint8_t buffer[2];
	uint8_t* buffer_ptr = buffer;
	TWI_read_register(devAddr, MPU6050_REG_GYRO_YOUT_H, buffer_ptr, 1);
	buffer_ptr++;
	TWI_read_register(devAddr, MPU6050_REG_GYRO_YOUT_L, buffer_ptr, 1);
	
	return (buffer[0] << 8 | buffer[1]);
}

int16_t MPU6050_get_gyroZ()
{
	uint8_t buffer[2];
	uint8_t* buffer_ptr = buffer;
	TWI_read_register(devAddr, MPU6050_REG_GYRO_ZOUT_H, buffer_ptr, 1);
	buffer_ptr++;
	TWI_read_register(devAddr, MPU6050_REG_GYRO_ZOUT_L, buffer_ptr, 1);
	
	return (buffer[0] << 8 | buffer[1]);
}
