#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/twi.h>

#include "twi.h"


void TWI_init()
{
	PORTC = (1 << PC5) | (1 << PC4);
	
	TWSR = 0;
	TWBR = 12;
	TWCR =  (1 << TWEN) | (1 << TWIE) | (1 << TWEA);	
}


void TWI_start()
{
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	TWI_wait();
	while((TWSR & 0xF8)!= 0x08);
}


void TWI_re_start()
{
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN); // re-start
	TWI_wait();
	while((TWSR & 0xF8) != 0x10);
}

void TWI_wait()
{
	while (!(TWCR & (1<<TWINT)));
}

void TWI_write(uint8_t byte)
{
	TWDR = byte;
	TWCR = (1<<TWINT) | (1<<TWEN);
	TWI_wait();
}

void TWI_stop()
{
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);
	while(!(TWCR & (1<<TWSTO)));
}

void TWI_read_register(uint8_t sla, uint8_t reg_addr, uint8_t* data, uint8_t len)
{
	for(int i = 0; i < len; i++)
	{
		TWI_start();
		TWI_write((sla << 1));
		TWI_write(reg_addr);
	
		TWI_re_start();
	
		TWI_write((sla << 1) + 1);
		while((TWSR & 0xF8) != 0x40);
	
		TWCR=(1<<TWINT)|(1<<TWEN);    // Clear TWI interrupt flag,Enable TWI
		while (!(TWCR & (1<<TWINT))); // Wait till complete TWDR byte transmitted

		data[i] = TWDR;
		while((TWSR & 0xF8) != 0x58);
	
		TWI_stop();
	}
}